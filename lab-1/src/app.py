"""
"""

import flask

import os


def create_app():
    """

    :return:
    """
    from src.config import init_db
    from src.config import init_ma

    app = flask.Flask(__name__)
    base_dir = os.path.abspath(os.path.dirname(__file__) + '/..')
    app.config['BASEDIR'] = base_dir

    # Database
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(
        base_dir, 'db.sqlite')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    init_db(app)
    init_ma(app)

    return app


app = create_app()

import src.routes

"""
# init app
app = flask.Flask(__name__)
base_dir = os.path.abspath(os.path.dirname(__file__))

# Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(
    base_dir, 'db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# initialize db
db = sql.SQLAlchemy(app)

# initialize marshmallow
ma = marshmallow.Marshmallow(app)

# Init schema
product_schema = models.ProductSchema()
products_schema = models.ProductSchema(many=True)
"""
