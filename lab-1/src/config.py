"""
"""

import flask_sqlalchemy
import flask_marshmallow
import werkzeug.security as security

db = flask_sqlalchemy.SQLAlchemy()
ma = flask_marshmallow.Marshmallow()

import src.models as models

product_schema = models.ProductSchema()
products_schema = models.ProductSchema(many=True)
user_schema = models.UserSchema()
users_schema = models.UserSchema(many=True)


def init_ma(app):
    """

    :param app:
    :return:
    """
    ma.init_app(app)


def init_db(app):
    """

    :return:
    """
    with app.app_context():
        db.init_app(app)
        db.create_all()
        add_admin()


def add_admin():
    """

    :return:
    """
    user = models.User.query.filter_by(username='admin').first()
    if user:
        return

    username = 'admin'
    password = 'admin'

    new_user = models.User(username, security.generate_password_hash(password))

    db.session.add(new_user)
    db.session.commit()
