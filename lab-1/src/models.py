"""
"""
from src.config import db, ma


class Product(db.Model):
    """
    """
    __tablename__ = "products"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    description = db.Column(db.String(200))
    price = db.Column(db.Float)
    qty = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('users.users_id'))

    def __init__(self, name, description, price, qty):
        """

        :param name:
        :param description:
        :param price:
        :param qty:
        """
        self.name = name
        self.description = description
        self.price = price
        self.qty = qty


# Product schema
class ProductSchema(ma.Schema):
    """
    """

    class Meta:
        fields = ('id', 'name', 'description', 'price', 'qty')


class User(db.Model):
    """
    """
    __tablename__ = "users"
    id = db.Column('users_id', db.Integer, primary_key=True)
    username = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    products = db.relationship("Product", backref='user', lazy='dynamic')

    def __init__(self, username, password):
        """

        :param id:
        :param username:
        :param password:
        :param products:
        """
        self.username = username
        self.password = password


class UserSchema(ma.Schema):
    """
    """

    class Meta:
        fields = ('id', 'username', 'password')
