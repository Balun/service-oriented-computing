"""
"""
import flask
import flask_httpauth
import werkzeug.security as security

from src.config import db, products_schema, product_schema, user_schema, \
    users_schema
from run import app
import src.models as models

auth = flask_httpauth.HTTPBasicAuth()


@app.errorhandler(404)
def not_found(error):
    return flask.make_response(flask.jsonify({'error': 'Not found'}), 404)


@auth.error_handler
def unauthorized():
    return flask.make_response(flask.jsonify({'error': 'Unauthorized'}), 401)


@auth.verify_password
def verify_password(username, password):
    """

    :param username:
    :param password:
    :return:
    """
    user = models.User.query.filter_by(username=username).first()

    if user:
        return security.check_password_hash(user.password, password)

    return False


@app.route('/')
@app.route('/index')
@auth.login_required
def index():
    return "Hello, %s!" % auth.username()


# Create a Product
@app.route('/user/<id>/product', methods=['POST'])
@auth.login_required
def add_product(id):
    """

    :return:
    """
    user = models.User.query.get(id)

    name = flask.request.json['name']
    description = flask.request.json['description']
    price = flask.request.json['price']
    qty = flask.request.json['qty']

    new_product = models.Product(name, description, price, qty)

    user.products.append(new_product)

    db.session.add(new_product)
    db.session.commit()

    return product_schema.jsonify(new_product), 201


# Get all products
@app.route('/product', methods=['GET'])
@auth.login_required
def get_products():
    """

    :return:
    """
    all_products = models.Product.query.all()
    result = products_schema.dump(all_products)

    return flask.jsonify(result)


# Get single product
@app.route('/product/<id>', methods=['GET'])
@auth.login_required
def get_product(id):
    """

    :param id:
    :return:
    """
    product = models.Product.query.get(id)
    return product_schema.jsonify(product)


# Get products by user
@app.route('/user/<id>/product', methods=['GET'])
def get_product_by_users(id):
    """

    :param id:
    :return:
    """
    user = models.User.query.get(id)

    products = user.products
    return products_schema.jsonify(products)


# Update a Product
@app.route('/product/<id>', methods=['PUT'])
@auth.login_required
def update_product(id):
    """

    :param id:
    :return:
    """
    product = models.Product.query.get(id)

    name = flask.request.json['name']
    description = flask.request.json['description']
    price = flask.request.json['price']
    qty = flask.request.json['qty']

    product.name = name
    product.description = description
    product.price = price
    product.qty = qty

    db.session.commit()

    return product_schema.jsonify(product)


# Delete single product
@app.route('/product/<id>', methods=['DELETE'])
@auth.login_required
def delete_product(id):
    """

    :param id:
    :return:
    """
    product = models.Product.query.get(id)
    db.session.delete(product)
    db.session.commit()

    return product_schema.jsonify(product)


# create a user
@app.route('/user', methods=['POST'])
@auth.login_required
def add_user():
    """

    :return:
    """
    username = flask.request.json['username']
    password = flask.request.json['password']

    new_user = models.User(username, security.generate_password_hash(password))

    db.session.add(new_user)
    db.session.commit()

    return user_schema.jsonify(new_user), 201


# get all users
@app.route('/user', methods=['GET'])
@auth.login_required
def get_users():
    """

    :return:
    """
    all_users = models.User.query.all()
    result = users_schema.dump(all_users)

    return flask.jsonify(result)


# Get single User
@app.route('/user/<id>', methods=['GET'])
@auth.login_required
def get_user(id):
    """

    :param id:
    :return:
    """
    user = models.User.query.get(id)
    return users_schema.jsonify(user)


# Update a user
@app.route('/user/<id>', methods=['PUT'])
@auth.login_required
def update_user(id):
    """

    :param id:
    :return:
    """
    user = models.User.query.get(id)

    username = flask.request.json['username']
    password = flask.request.json['password']

    user.username = username
    user.password = security.generate_password_hash(password)

    db.session.commit()

    return user_schema.jsonify(user)


# Delete a user
@app.route('/user/<id>', methods=["DELETE"])
@auth.login_required
def delete_user(id):
    """

    :param id:
    :return:
    """
    user = models.User.query.get(id)
    db.session.delete(user)
    db.session.commit()

    return user_schema.jsonify(user)
