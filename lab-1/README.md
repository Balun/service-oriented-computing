## Lab 1: Creating a simple RESTful API
To Run the application, execute the script "run.py".
The application is running on [http://127.0.0.1:5000/]

### List of URLs and CRUD methods
#### 1. "/", "/index" 
* **GET**  
    - Returns the 200 OK "Hallo User" message if authorization was successful
    - Returns 401 UNAUTHORIZED otherwise
           
#### 2. "/user"
* **GET**   
    - Returns all defined users, 200 OK
* **POST**
    - 201 OK, creates the new User with data specified in the request
   
#### 3. "/user/id"
* **GET**
     - Returns the user with the specified id, 200 OK
* **PUT**
     - Updates the user with the specified id using data from request, 200 OK
* **DELETE**
    - Deletes the user with the specified id, 200 OK
 
#### 4. "/product"
* **GET**
    - Returns all defined products, 200 OK

#### 5. "/product/id"
* **GET**
     - Returns the specified product, 200 OK
* **PUT**
     - Updates the specified product with data from request, 200 OK
* **DELETE**
     - Deletes the specified product

#### 6. "/user/id/product"
* **GET**
    - Returns all defined products for specified user, 200 OK
* **POST**
    - Creates the new product for the specified user, 201 CREATED
    
For all the URLs and methods not listed here, the expected response is 405
 METHOD NOT ALLOWED