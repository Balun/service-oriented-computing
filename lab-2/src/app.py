"""
"""

import flask

import os


def create_app():
    """
    :return:

    """
    from src.config import init_ma
    from src.config import init_db
    from src.config import init_socket

    # app initialization
    base_dir = os.path.abspath(os.getcwd())
    app = flask.Flask(__name__, template_folder=os.path.join(base_dir,
                                                             'res/templates'))

    # app configuration
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(
        base_dir,
        'db.sqlite')
    app.config['SECRET_KEY'] = 'mysecret'

    init_db(app)
    init_ma(app)
    init_socket(app)

    return app


app = create_app()

import src.routes
