"""
"""

from src.config import db, ma


class History(db.Model):
    """
    """

    id = db.Column('messages_id', db.Integer, primary_key=True)
    message = db.Column('message', db.String(500))
    author = db.Column('author', db.String(500))

    def __init__(self, message, author):
        """

        :param message:
        :param author:
        """
        self.message = message
        self.author = author


class HistorySchema(ma.Schema):
    """
    """

    class Meta:
        fields = ('id', 'message', 'author')


class User(db.Model):
    """
    """

    id = db.Column('users_id', db.Integer, primary_key=True)
    username = db.Column('username', db.String(500), unique=True)
    password = db.Column(db.String(100))

    def __init__(self, username, password):
        """

        :param id:
        :param username:
        :param products:
        """
        self.username = username
        self.password = password


class UserSchema(ma.Schema):
    """
    """

    class Meta:
        fields = ('id', 'username', 'password')
