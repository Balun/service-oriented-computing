from src.config import db
from src.app import app
from src.config import socket

import src.models as models

from flask import render_template
from flask_socketio import send
from src.config import db, user_schema, users_schema

import werkzeug.security as security

import flask
import flask_httpauth

import os

auth = flask_httpauth.HTTPBasicAuth()


@auth.error_handler
def unauthorized():
    return flask.make_response(flask.jsonify({'error': 'Unauthorized'}), 401)


@auth.verify_password
def verify_password(username, password):
    """

    :param username:
    :param password:
    :return:
    """
    user = models.User.query.filter_by(username=username).first()

    if user:
        res = security.check_password_hash(user.password, password)
        if res:
            flask.session['user'] = str(user.username)
            return True

    return False


@socket.on('message')
@auth.login_required
def handleMessage(msg):
    """

    :param msg:
    :return:
    """
    user = flask.session['user']
    print('Message: ' + msg)
    print("User: " + user)

    message = models.History(message=msg, author=user)
    db.session.add(message)
    db.session.commit()

    send(msg, broadcast=True)


@app.route('/')
@app.route('/index')
@auth.login_required
def sessions():
    """

    :return:
    """
    print(os.getcwd)
    messages = models.History.query.all()
    return render_template('index.html', messages=messages)


# create a user
@app.route('/user', methods=['POST'])
def add_user():
    """

    :return:
    """
    username = flask.request.json['username']
    password = flask.request.json['password']

    new_user = models.User(username, security.generate_password_hash(password))

    db.session.add(new_user)
    db.session.commit()

    return user_schema.jsonify(new_user), 201


# get all users
@app.route('/user', methods=['GET'])
def get_users():
    """

    :return:
    """
    all_users = models.User.query.all()
    result = users_schema.dump(all_users)

    return flask.jsonify(result)
