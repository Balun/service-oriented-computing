"""
"""
from src.app import app
from src.config import socket

if __name__ == '__main__':
    socket.run(app, debug=True)