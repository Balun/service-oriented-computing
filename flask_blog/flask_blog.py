import flask
import forms

app = flask.Flask(__name__)

app.config['SECRET_KEY'] = 'b84b70723b6f326fd60a5c27f2c2774b'

posts = [
    {
        'author': "Matej Balun",
        'title': "Blog 1",
        'content': "Prvi blog",
        "date_posted": "Amripl 20, 2018."
    },
    {
        'author': "Borna Balun",
        'title': "Blog 2",
        'content': "Drugi blog",
        "date_posted": "Amripl 21, 2018."
    }
]


@app.route("/")
@app.route("/home")
def home():
    return flask.render_template('home.html', posts=posts)


@app.route("/about")
def about():
    return flask.render_template('about.html', title="About")


@app.route("/register", methods=['GET', "POST"])
def register():
    form = forms.RegistrationForm()
    if form.validate_on_submit():
        flask.flash(f'Account created for {form.username.data}!', 'success')
        return flask.redirect(flask.url_for('home'))

    return flask.render_template('register.html', title='Register',
                                 form=form)


@app.route("/login", methods=['GET', "POST"])
def login():
    form = forms.LoginForm()

    if form.validate_on_submit():
        if form.email.data == 'admin@blog.com' and form.password.data == \
                'password':
            flask.flash("You have been logged in!", 'success')
            return flask.redirect(flask.url_for('home'))

        else:
            flask.flash("Login unscuccesseful. Check username and password",
                        'danger')

    return flask.render_template('login.html', title='Login',
                                 form=form)


if __name__ == '__main__':
    app.run(debug=True)
