import flask_wtf
import wtforms
import wtforms.validators as valid


class RegistrationForm(flask_wtf.FlaskForm):
    """
    """

    username = wtforms.StringField('Username', validators=[
        valid.DataRequired(), valid.Length(min=2, max=20)])

    email = wtforms.StringField('Email', validators=[valid.DataRequired(),
                                                     valid.Email()])

    password = wtforms.PasswordField('Password', validators=[
        valid.DataRequired()])

    confirm_password = wtforms.PasswordField('Confirm password', validators=[
        valid.DataRequired(), valid.EqualTo('password')])

    submit = wtforms.SubmitField('Sign Up')


class LoginForm(flask_wtf.FlaskForm):
    """
    """
    email = wtforms.StringField('Email', validators=[valid.DataRequired(),
                                                     valid.Email()])

    password = wtforms.PasswordField('Password', validators=[
        valid.DataRequired()])

    remember = wtforms.BooleanField("Remember me")

    submit = wtforms.SubmitField('Login')
